Following is the directory structure
* example - contains the example html / JS file that is integrated with our speech SDK
* example/test.html 
* example/test.js - the test.js initialises the SpeechRecogniser and sets relevant callbacks to handle the speech processing.
* lib - contains the speech recogniser library of prayagad


#### Basic approach of sample application
In speech there are generally 2 parts. One is interim text and other is final text. Interim text is given as and when the speaker is speaking. 
The interim text can change as the system hears more and more speech. This is showed to the user to give him a sense that application is 
listening to the user and performing speech recognition

Final text is given by the system once user makes a pause or stops recording. This final text is the one that should be used for all processing purposes.
This text will not change after this and is considered stable translation of speech.

In the sample application the top text box shows the interim text as and when the user speaks. Once part of that becomes final it puts the final text also 
as part of the sample text box and continues to append the interim text that may come after that.

The update to the text box is all done by setting relevant callback functions that speech system will call. These are the callbacks that is set 
by the application and speech SDK will call these.

The example app has only 2 sections. One is chief complaint and other is medication. If user says medication then all text after will be set in medicaiton box. Otherwise by deffault 
all speech text will be part of chief complaint section.

