import SpeechRecognition from './speechRecognitionLib';
import SpeechProcessor from './speechRecognitionLib/speechProcessor';

class SpeechRecognizer {
  recognition = null;

  // Listener Fns
  interimTextListener = (text) => {
    console.log('Interim Text Incoming :' + text);
    console.log('This function is not implemented yet');
  };
  finalTextListener = (finalSectionTranscript) => {
    console.log('Section Transcript :' + finalSectionTranscript);
    console.log('This function is not implemented yet');
  };
  detectSectionListener = (sectionName) => {
    console.log('Detected Section' + sectionName);
    console.log('This function is not implemented yet');
  };

  startSectionName = '';
  phraseSectionDict = {};

  constructor(lang = 'en-IN', interimResults = true, continuous = true, maxAlternatives = 5) {
    this.recognition = new SpeechRecognition();
    this.recognition.continuous = continuous;
    this.recognition.interimResults = interimResults;
    this.recognition.maxAlternatives = maxAlternatives;
    this.recognition.lang = lang;
    this.recognition.on('results', this.processResults);
    this.recognition.onresult = this.processResults;
    this.lastSection = 'chiefComplaint';
    console.log('constructor: web speech recorder initalized', this.recognition);
  }

  setDefaultSection = (sectionName) => {
    this.lastSection = sectionName;
  }

  addSectionPhrase = (section, phrase) => {
    if (this.phraseSectionDict != null) {
      this.phraseSectionDict[phrase] = section;
    } else {
      this.phraseSectionDict = {};
      this.phraseSectionDict[phrase] = section;
    }
  }

  processResults = (speechResponse) => {

    // Getting the relevant results
    let transcript = SpeechProcessor.processTranscript(speechResponse);

    // Checking if its a interim text or final text
    let isFinal = speechResponse.results[0].isFinal;

    // Extracting all the speech phrases
    let speechPhrases = [];
    let phrases = Object.keys(this.phraseSectionDict);

    // Processing final Text
    if (isFinal === true) {

      console.log('processResults: final transcript', transcript);

      // All data corresponding to different sections wiould be stored here
      let finalSectionTranscript = {};

      let matchingPhrases = phrases.filter((phrase) => { return transcript.trim().toLowerCase().includes(phrase.trim().toLowerCase()); });
      console.log('processResults:  final matchingPhrases', matchingPhrases);

      let tokens = [{ value: transcript, tag: 'text' }];

      if (matchingPhrases != null && matchingPhrases.length > 0) {
        transcript = this.startSectionName + ' ' + transcript;

        // Trimming the transcript with the matchingPhrases. Emit the transcript and continue scanning for phrases
        matchingPhrases.forEach((phrase) => {
          const newTokens = [];
          tokens.forEach((currentToken) => {
            const { value, tag } = currentToken;
            if (tag === 'phrase') {
              newTokens.push(currentToken);
            } else {
              const splits = value.toLowerCase().split(phrase);
              const noSplits = splits.length;
              splits.forEach((split, index) => {
                newTokens.push({ value: split, tag: 'text' });
                if (index < noSplits - 1) {
                  newTokens.push({ value: phrase, tag: 'phrase' });
                }
              });
            }
          });
          tokens = newTokens;
        });
      }

      let currentSection = (this.startSectionName === undefined || this.startSectionName === '') ? this.lastSection : this.startSectionName;
      console.log('processResults: final tokens', tokens);
      tokens.forEach((currentToken) => {
        const { value, tag } = currentToken;
        if (tag === 'phrase') {
          const section = this.phraseSectionDict[value];
          if (section != null) {
            currentSection = section;
          }
        } else {
          if (finalSectionTranscript[currentSection] == null) {
            finalSectionTranscript[currentSection] = value;
          } else {
            finalSectionTranscript[currentSection] = finalSectionTranscript[currentSection] + ' ' + value.trim();
          }
        }
      });
      this.finalTextListener(finalSectionTranscript);
    } else {

      // For interim transcript , if we encounter a matching phrase for a section , we should trigger detectSectionListenedr
      console.log("processResults: interim transcript", transcript);
      let matchingPhrases = phrases.filter((phrase) => { return transcript.trim().toLowerCase().includes(phrase.trim().toLowerCase()); });
      if (matchingPhrases != null && matchingPhrases.length > 0) {
        matchingPhrases.forEach(phrase => {
          this.detectSectionListener(this.phraseSectionDict[phrase]);
          // set this section as the last section so that this is used incase there is no section name
          this.lastSection = this.phraseSectionDict[phrase];
        });
      }
      this.interimTextListener(transcript);
      console.log('processResults: matchingPhrases', matchingPhrases);
    }

  }

  addStartListener = (listnenerFn) => {
    this.recognition.on('start', listnenerFn);
    this.recognition.onstart = listnenerFn;
  };

  addStopListener = (listnenerFn) => {
    this.recognition.on('end', listnenerFn);
    this.recognition.onend = listnenerFn;
  };

  addFinalTextListener = (listnenerFn) => {
    this.finalTextListener = listnenerFn;
  };


  addInterimTextListener = (listnenerFn) => {
    this.interimTextListener = listnenerFn;
  };

  addDetectSectionListener = (listnenerFn) => {
    this.detectSectionListener = listnenerFn;
  };

  addErrorListener = (listnenerFn) => {
    this.recognition.on('error', listnenerFn);
    this.recognition.onerror = listnenerFn;
  };

  startVoiceCapture = (recordingId = null, documentId = null) => {
    try {

      this.recognition.start({ recordingId: recordingId, documentId: documentId });
      console.log("startVoiceCapture:starting");
      return true;
    }
    catch (error) {
      console.log("startVoiceCapture: error starting", error);
      return false;
    }
  };

  stopVoiceCapture = () => {
    try {
      this.recognition.stop();
      return true;
    } catch (error) {
      console.log("stopVoiceCapture: error stopping", error);
      return false;
    }
  };


  destroy = () => {
    if (this.recognition != null) {
      this.recognition.stop();
      this.recognition.destory();
    }
    this.recognition = null;
  }
}
export default SpeechRecognizer;

