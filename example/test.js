var sr = new SpeechRecognizer();
var chiefComplaintText = '';
var medicationText = '';
var computedFinalText = '';
var micCurState = 'off';

function stopListening() {
  micCurState = 'off';
  $("#micImage").attr("src", "./images/ico_micoff.png");
}

function startListening() {
  micCurState = 'on';
  $("#micImage").attr("src", "./images/ico_micon.png");
}

function handleMicState() {
  if (micCurState === 'off') {
    sr.startVoiceCapture();
  } else {
    sr.stopVoiceCapture();
  }
};

function handleSectionChange(sectionName) {
  console.log(`****** current active section: ${sectionName} ******`);
  // do any switch to different page based on section name here
}

function handleFinalText(finalTextObject) {
  $.each(finalTextObject, function(key, value){
    computedFinalText = computedFinalText + value + ' ';
    if (key === 'chiefComplaint') {
      chiefComplaintText = chiefComplaintText + value + ' ';
    } else if (key === 'medication') {
      medicationText = medicationText + value + ' ';
    }
  });
  console.log(`final computed text: ${computedFinalText}`);
  $("#speechText").val(computedFinalText);
  $("#chiefComplaint").val(chiefComplaintText);
  $("#medication").val(medicationText);
}

function handleInterimText(text) {
  console.log(`current final text: ${computedFinalText}`);
  console.log(`interim text: ${text}`);
  var displayText = computedFinalText + text;
  console.log(`display text: ${text}`);
  $("#speechText").val(displayText);
};

$(document).ready(function () {
  sr.addSectionPhrase('medication', 'medication');
  sr.addSectionPhrase('medication', 'medicine advice');
  sr.addSectionPhrase('investigation', 'investigation')
  sr.addSectionPhrase('chiefComplaint', 'chief complaint')
  sr.addSectionPhrase('chiefComplaint', 'primary complaint')
  sr.addSectionPhrase('chiefComplaint', 'subject')
  sr.addStopListener(stopListening);
  sr.addStartListener(startListening);

  sr.addInterimTextListener(handleInterimText);
  sr.addFinalTextListener(handleFinalText);
  sr.addDetectSectionListener(handleSectionChange);

  $("#micState").on('click', handleMicState);
});

